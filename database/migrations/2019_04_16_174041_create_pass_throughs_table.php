<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePassThroughsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pass_throughs', function (Blueprint $table) {
            $table->bigIncrements('id')->unique();
            $table->bigInteger('association_id')->unsigned();
            $table->date('payment_date');
            $table->date('competence');
            $table->bigInteger('pass_through_plan_id')->unsigned();
            $table->integer('holders_amount');
            $table->integer('unitary_value');
            $table->integer('extra_value');
            $table->integer('total_value');
            $table->bigInteger('pass_through_payment_type_id')->unsigned();
            $table->bigInteger('pass_through_status_id')->unsigned();
            $table->string('document');
            $table->timestamps();
        });
        Schema::table('pass_throughs', function(Blueprint $table) {
            $table->foreign('association_id')->references('id')->on('associations');
            $table->foreign('pass_through_payment_type_id')->references('id')->on('pass_through_payment_types');
            $table->foreign('pass_through_status_id')->references('id')->on('pass_through_statuses');
            $table->foreign('pass_through_plan_id')->references('id')->on('pass_through_plans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pass_throughs');
    }
}
