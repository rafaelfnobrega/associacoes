<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssociationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associations', function (Blueprint $table) {
            $table->bigIncrements('id')->unique();
            $table->string('initials');
            $table->string('name');
            $table->string('cnpj');
            $table->string('campus');
            $table->string('cep');
            $table->string('full_adress');
            $table->string('complement');
            $table->string('neighborhood');
            $table->string('city');
            $table->string('state');
            $table->string('phone');
            $table->string('phone2');
            $table->string('cellphone');
            $table->string('email');
            $table->string('email2');
            $table->string('contact');
            $table->bigInteger('association_status_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('associations', function (Blueprint $table) {
            $table->foreign('association_status_id')->references('id')->on('association_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associations');
    }
}
