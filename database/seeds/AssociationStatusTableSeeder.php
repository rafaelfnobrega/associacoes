<?php

use Illuminate\Database\Seeder;

class AssociationStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('association_statuses')->insert([
            'description'=>'Ativo',
        ]);
        DB::table('association_statuses')->insert([
            'description'=>'Inativo',
        ]);
    }
}