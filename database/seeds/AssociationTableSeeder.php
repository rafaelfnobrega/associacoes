<?php

use Illuminate\Database\Seeder;

class AssociationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('associations')->insert([
            'initials' => 'ASCAR',
            'name' => 'Associação ascar',
            'cnpj' => '1235315423',
            'campus' => 'campus',
            'cep' => '123456-89',
            'full_adress' => 'rua da ascar',
            'complement' => 'dentro do campus',
            'neighborhood' => 'bairro',
            'city' => 'cidade',
            'state' => 'estado',
            'phone' => '(12) 34567-8900',
            'phone2' => '(12) 34567-8901',
            'cellphone' => '(12) 34567-8902',
            'email' => 'email@ascar.com',
            'email2' => 'email2@ascar.com',
            'contact' => 'contato na ascar',
            'association_status_id' => 1,
        ]);
        DB::table('associations')->insert([
            'initials' => 'ASA',
            'name' => 'Associação ascar',
            'cnpj' => '1235315423',
            'campus' => 'campus',
            'cep' => '123456-89',
            'full_adress' => 'rua da ascar',
            'complement' => 'dentro do campus',
            'neighborhood' => 'bairro',
            'city' => 'cidade',
            'state' => 'estado',
            'phone' => '(12) 34567-8900',
            'phone2' => '(12) 34567-8901',
            'cellphone' => '(12) 34567-8902',
            'email' => 'email@ascar.com',
            'email2' => 'email2@ascar.com',
            'contact' => 'contato na ascar',
            'association_status_id' => 2,
        ]);
    }
}
