<?php

use Illuminate\Database\Seeder;

class PassThroughStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pass_through_statuses')->insert([
            'description'=>'Pago',
        ]);
        DB::table('pass_through_statuses')->insert([
            'description'=>'Em aberto',
        ]);
        DB::table('pass_through_statuses')->insert([
            'description'=>'Outros',
        ]);
    }
}
