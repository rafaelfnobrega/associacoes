<?php

use Illuminate\Database\Seeder;

class PassThroughPlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pass_through_plans')->insert([
            'description'=>'Saúde',
        ]);
        DB::table('pass_through_plans')->insert([
            'description'=>'Odonto',
        ]);
    }
}
