<?php

use Illuminate\Database\Seeder;

class PassThroughTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pass_throughs')->insert([
            'association_id'=>1,
            'payment_date'=>\Carbon\Carbon::now(),
            'competence'=>\Carbon\Carbon::now(),
            'pass_through_plan_id'=>1,
            'holders_amount'=>100,
            'unitary_value'=>1,
            'extra_value'=>0,
            'total_value'=>100,
            'pass_through_payment_type_id'=>1,
            'pass_through_status_id'=>1,
            'document'=>'123abc',
        ]);
    }
}
