<?php

use Illuminate\Database\Seeder;

class PassThroughPaymentTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pass_through_payment_types')->insert([
            'description'=>'TED',
        ]);
        DB::table('pass_through_payment_types')->insert([
            'description'=>'Depósito',
        ]);
        DB::table('pass_through_payment_types')->insert([
            'description'=>'Outros',
        ]);
    }
}
