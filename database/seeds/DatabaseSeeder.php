<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(AssociationStatusTableSeeder::class);
        $this->call(AssociationTableSeeder::class);
        $this->call(PassThroughPaymentTypeTableSeeder::class);
        $this->call(PassThroughStatusTableSeeder::class);
        $this->call(PassThroughPlanTableSeeder::class);
        $this->call(PassThroughTableSeeder::class);
    }
}
