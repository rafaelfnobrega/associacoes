<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Rafael Farias da Nóbrega",
            "email" => "rafael@e8.com.br",
            "password" =>bcrypt('senha123'),
        ]);
        DB::table('users')->insert([
            "name" => "Juliano Farias da Nóbrega",
            "email" => "juliano@e8.com.br",
            "password" => bcrypt('senha2019'),
        ]);
    }
}
