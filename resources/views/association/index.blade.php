@extends('adminlte::page')

@section('content_header')
@endsection

@section('content')

@push('css')
<style>
    .uper {
        margin-top: 40px;
    }
</style>
@if(session()->get('success'))
    <div class="alert alert-success">
    {{ session()->get('success') }}  
    </div><br />
@endif
<div class="col-md-6">
    
    <div class="box box-primary">
        <div class="box-header with-border">
            <h2 class="box-title">Associações ativas</h2>
            
        </div>
        <div class="box-body">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Sigla</th>
                        <th>Nome</th>
                        <th width="40"> </th>
                    </tr>
                </thead>
                @foreach($associations as $association)
                    <tr>
                        <td> {{$association->initials}} </td>
                        <td> {{$association->name}} </td>
                        <td>
                            <a class="table-row btn btn-primary" href="{{ route('associations.show',Hashids::connection('association')->encode($association->id))}}">
                                Detalhes
                            </a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="box box-success">
        <div class="box-header with-border">
            <h2 class="box-title">Pesquisar(em construção)</h2>
        </div>
        <div class="box-body with-border">
            <div class="form-group">
                <input class="form-control" type="text" name="pesquisar" id="" placeholder="Pesquisar cliente">
            </div>
        </div>
        <div class="box-header with-border">
            <h2 class="box-title">Ações</h2>
        </div>
        <div class="box-body with-border">
            <a href={{ route('associations.create')}} class="btn btn-success">
                <span class="glyphicon glyphicon-plus"></span>
                Novo
            </a>
        </div>
        <div class="box-header with-border">
            <h2 class="box-title">Listar(em construção)</h2>
        </div>
        <div class="box-body">
            <a href="#" class="btn btn-danger">
                Inativos
            </a>
            <a href="#" class="btn btn-success">
                Ativos
            </a>
            <a href="#" class="btn btn-warning">
                Inadimplentes
            </a>
            <a href="#" class="btn btn-primary">
                Maiores
            </a>

        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection