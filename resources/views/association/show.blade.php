@extends('association.layout')

@section('content_header')
@endsection

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
  td:first-child{
      text-align:right;
      width:150px;
  }
</style>
<div class="col-md-6">
    <div class="box box-danger">
        <div class="box-header">
            <h2 class="box-title">Detalhes Associação</h2>
            @if(!empty($success))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ $success }}  
                </div>
            @endif
        </div>
        <div class="box-body">
            <table class="table table-striped table-hover">
                <tbody>
                    <tr>
                        <td align="right">Sigla</td>
                        <td>{{$association->initials}}</td>
                    </tr>
                    <tr>
                        <td align="right">Nome</td>
                        <td>{{$association->name}}</td>
                    </tr>
                    <tr>
                        <td align="right">CNPJ</td>
                        <td>{{$association->cnpj}}</td>
                    </tr>
                    <tr>
                        <td align="right">Campus</td>
                        <td>{{$association->campus}}</td>
                    </tr>
                    <tr>
                        <td align="right">CEP</td>
                        <td>{{$association->cep}}</td>
                    </tr>
                    <tr>
                        <td align="right">Endereço + número</td>
                        <td>{{$association->full_adress}}</td>
                    </tr>
                    <tr>
                        <td align="right">Complemento</td>
                        <td>{{$association->complement}}</td>
                    </tr>
                    <tr>
                        <td align="right">Bairro</td>
                        <td>{{$association->neighborhood}}</td>
                    </tr>
                    <tr>
                        <td align="right">Cidade</td>
                        <td>{{$association->city}}</td>
                    </tr>
                    <tr>
                        <td align="right">UF</td>
                        <td>{{$association->state}}</td>
                    </tr>
                    <tr>
                        <td align="right">Telefone</td>
                        <td>{{$association->phone}}</td>
                    </tr>
                    <tr>
                        <td align="right">Telefone 2</td>
                        <td>{{$association->phone2}}</td>
                    </tr>
                    <tr>
                        <td align="right">Celular</td>
                        <td>{{$association->cellphone}}</td>
                    </tr>
                    <tr>
                        <td align="right">Email</td>
                        <td>{{$association->email}}</td>
                    </tr>
                    <tr>
                        <td align="right">Email 2</td>
                        <td>{{$association->email2}}</td>
                    </tr>
                    <tr>
                        <td align="right">Contato</td>
                        <td>{{$association->contact}}</td>
                    </tr>
                    <tr>
                        <td align="right">Status</td>
                        <td>{{$association->associationStatus->description}}</td>
                    </tr>
                    {{-- <tr>
                        <td align='right'>Cadastrado por:</td>
                        <td>{{$association->registerUser->name}}</td>
                    </tr>
                    <tr>
                        <td align='right'>Cadastrado em:</td>
                        <td>{{$association->created_at->format('d/m/Y H:i:s')}}</td>
                    </tr> --}}
                    <tr>
                        <td align='right'></td>
                        <td>
                            <a href="{{ route('associations.edit',Hashids::connection('association')->encode($association->id))}}" class="btn btn-primary">Editar</a>
                        </td>
                    </tr>
                </tbody>
                
            </table>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection