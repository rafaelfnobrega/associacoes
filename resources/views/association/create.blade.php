@extends('adminlte::page')


@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"
integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
crossorigin="anonymous"></script>
<!-- Adicionando Javascript -->
<script type="text/javascript" >

    $(document).ready(function() {

        function limpa_formulário_cep() {
            // Limpa valores do formulário de cep
            $("#rua").val("");
            $("#bairro").val("");
            $("#cidade").val("");
            $("#uf").val("");
            //$("#ibge").val("");
        }
        
        //Quando o campo cep perde o foco
        $("#cep").blur(function() {

            //Nova variável "cep" somente com dígitos
            var cep = $(this).val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado
            if (cep != "") {

                //Expressão regular para validar o CEP
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                    //Preenche os campos com "..." enquanto consulta webservice
                    $("#rua").val("...");
                    $("#bairro").val("...");
                    $("#cidade").val("...");
                    $("#uf").val("...");
                    //$("#ibge").val("...");

                    //Consulta o webservice viacep.com.br/
                    $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#rua").val(dados.logradouro);
                            $("#bairro").val(dados.bairro);
                            $("#cidade").val(dados.localidade);
                            // $('#uf option[value='+dados.uf+']').eq(optionIndex).prop('selected',true);
                            $("#uf").val(dados.uf);
                            //$("#ibge").val(dados.ibge);
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            limpa_formulário_cep();
                            alert("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulário_cep();
                    alert("Formato de CEP inválido.");
                } 
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                limpa_formulário_cep();
            }
        });

        //SP CELPHONES
        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };

        $('.sp_celphones').mask(SPMaskBehavior, spOptions);
        
        //CPF/CNPJ
        var comportamentoCpfCnpj = function (val) {
            return val.replace(/\D/g, '').length < 12 ?  '000.000.000-009999':'000.000.000/0099-99';
        },
        cpfCnpjOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(comportamentoCpfCnpj.apply({}, arguments), options);
            }
        };

        $('.cpf_cnpj').mask(comportamentoCpfCnpj, cpfCnpjOptions);
    });

</script>
<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h2 class="box-title">Nova associação</h2>
        </div>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <form method="post" action="{{ route('associations.store') }}">
            <div class="box-body">
                <div class="form-group {{ $errors->has('initials') ? 'has-error' : '' }}">
                    @csrf

                    <label for="initials"> Sigla</label>
                    <input 
                        type="text" 
                        class="form-control" 
                        name="initials"
                        value="{{old('initials')}}"
                    />
                    @if ($errors->has('initials'))
                        <span class="help-block">
                            <strong>{{ $errors->first('initials') }}</strong>
                        </span>
                    @endif
                </div>
                    
                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                    <label for="name"> Nome</label>
                    <input 
                        type="text" 
                        class="form-control" 
                        name="name"
                        value="{{old('name')}}"
                    />

                    @if($errors->has('name'))
                        <span class="help-block">
                            <strong>{{$errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                    
                <div class="form-group">
                    <label for="cnpj"> CNPJ</label>
                    <input
                        type="text"
                        class="form-control cnpj"
                        name="cnpj"
                        value="{{old('cnpj')}}"
                    />
                </div>
                    
                <div class="form-group">
                    <label for="campus"> Campus</label>
                    <input
                        type="text"
                        class="form-control"
                        name="campus"
                        value="{{old('campus')}}"
                    />
                </div>
                    
                <div class="form-group">
                    <label for="cep"> Cep</label>
                    <input
                        type="text"
                        class="form-control"
                        id="cep"
                        name="cep"
                        value="{{old('cep')}}"
                        data-mask="00000-000"
                    />
                </div>
                    
                <div class="form-group">
                    <label for="full_adress"> Endereço + número</label>
                    <input
                        type="text"
                        class="form-control"
                        id="rua"
                        name="full_adress"
                        value="{{old('full_adress')}}"
                    />
                </div>
                    
                <div class="form-group">
                    <label for="complement"> Complemento:</label>
                    <input
                        type="text"
                        class="form-control"
                        name="complement"
                        value="{{old('complement')}}"
                    />
                </div>
                    
                <div class="form-group">
                    <label for="neighborhood"> Bairro</label>
                    <input
                        type="text"
                        class="form-control"
                        name="neighborhood"
                        id="bairro"
                        value="{{old('neighborhood')}}"
                    />
                </div>
                    
                <div class="form-group">
                    <label for="city"> Cidade</label>
                    <input
                        type="text"
                        class="form-control"
                        name="city"
                        id="cidade"
                        value="{{old('city')}}"
                    />
                </div>
                    
                <div class="form-group">
                    <label for="state"> UF</label>
                    <input
                        type="text"
                        class="form-control"
                        id="uf"
                        name="state"
                        data-mask="AA"
                        value="{{old('state')}}"
                    />
                </div>
                    
                <div class="form-group">
                    <label for="phone"> Telefone</label>
                    <input
                        type="text"
                        class="form-control sp_celphones"
                        name="phone"
                        value="{{old('phone')}}"
                    />
                </div>

                <div class="form-group">
                    <label for="phone2"> Telefone 2</label>
                    <input
                        type="text"
                        class="form-control sp_celphone2s"
                        name="phone2"
                        value="{{old('phone2')}}"
                    />
                </div>
                    
                <div class="form-group">
                    <label for="cellphone"> Celular</label>
                    <input
                        type="text"
                        class="form-control sp_celphones"
                        name="cellphone"
                        value="{{old('cellphone')}}"
                    />
                </div>
                    
                <div class="form-group">
                    <label for="email"> Email</label>
                    <input
                        type="text"
                        class="form-control"
                        name="email"
                        value="{{old('email')}}"
                    />
                </div>
                    
                <div class="form-group">
                    <label for="email2"> Email 2</label>
                    <input
                        type="text"
                        class="form-control"
                        name="email2"
                        value="{{old('email2')}}"
                    />
                </div>
                    
                <div class="form-group">
                    <label for="contact"> Contato</label>
                    <input
                        type="text"
                        class="form-control"
                        name="contact"
                        value="{{old('contact')}}"
                    />
                </div>

                <button type="submit" class="btn btn-primary">Adicionar</button>
            </div>
        </form>
    </div>
</div>
<div class="clearfix"></div>
@endsection