@extends('adminlte::page')
@section('content')

  <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
  @yield('content_client')
  <div class="clearfix"></div>
  <script src="{{ asset('js/app.js') }}" type="text/js"></script>


@endsection