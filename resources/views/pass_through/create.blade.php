@extends('adminlte::page')


@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"
integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
crossorigin="anonymous"></script>

<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h2 class="box-title">Novo repasse</h2>
        </div>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <form method="post" action="{{ route('associations.store') }}">
            <div class="box-body">
                
                @csrf
                <div class="form-group">
                    <label for="association_id">Associação</label>
                    <select name="association_id" id="association_id" class="form-control">
                        <option value="">Selecione</option>

                        @foreach(App\Association::all() as $association)
                            <option value="{{$association->id}}"
                                @if($association->id==old('association_id'))
                                    selected
                                @endif
                                >
                                {{$association->initials}}
                                -
                                {{$association->name}}
                            </option>
                        @endforeach

                    </select>
                </div>
                    
                <div class="form-group {{ $errors->has('payment_date') ? 'has-error' : ''}}">
                    <label for="payment_date"> Data Pagamento</label>
                    <input 
                        type="date" 
                        class="form-control" 
                        name="payment_date"
                        value="{{old('payment_date')}}"
                    />

                    @if($errors->has('payment_date'))
                        <span class="help-block">
                            <strong>{{$errors->first('payment_date') }}</strong>
                        </span>
                    @endif
                </div>
                
                <div class="form-group {{ $errors->has('competence') ? 'has-error' : ''}}">
                    <label for="competence"> Data Competência</label>
                    <input 
                        type="date" 
                        class="form-control" 
                        name="competence"
                        value="{{old('competence')}}"
                    />

                    @if($errors->has('competence'))
                        <span class="help-block">
                            <strong>{{$errors->first('competence') }}</strong>
                        </span>
                    @endif
                </div>
                    
                <div class="form-group">
                    <label for="pass_through_plan_id">Plano</label>
                    <select name="pass_through_plan_id" id="pass_through_plan_id" class="form-control">
                        <option value="">Selecione</option>

                        @foreach(App\PassThroughPlan::all() as $pass_through_plan)
                            <option value="{{$pass_through_plan->id}}"
                                @if($pass_through_plan->id==old('pass_through_plan_id'))
                                    selected
                                @endif
                                >
                                {{$pass_through_plan->description}}
                            </option>
                        @endforeach

                    </select>
                </div>
                    
                <div class="form-group {{ $errors->has('holders_amount') ? 'has-error' : ''}}">
                    <label for="holders_amount"> Quantidade de titulares</label>
                    <input 
                        type="number" 
                        class="form-control" 
                        name="holders_amount"
                        value="{{old('holders_amount')}}"
                    />

                    @if($errors->has('holders_amount'))
                        <span class="help-block">
                            <strong>{{$errors->first('holders_amount') }}</strong>
                        </span>
                    @endif
                </div>
                    
                <div class="form-group {{ $errors->has('unitary_value') ? 'has-error' : ''}}">
                    <label for="unitary_value"> Valor unitário</label>
                    <input 
                        type="text" 
                        class="form-control valor" 
                        name="unitary_value"
                        value="{{old('unitary_value')}}"
                    />

                    @if($errors->has('unitary_value'))
                        <span class="help-block">
                            <strong>{{$errors->first('unitary_value') }}</strong>
                        </span>
                    @endif
                </div>
                    
                <div class="form-group {{ $errors->has('extra_value') ? 'has-error' : ''}}">
                    <label for="extra_value"> Valor extra</label>
                    <input 
                        type="text" 
                        class="form-control valor" 
                        name="extra_value"
                        value="{{old('extra_value')}}"
                    />

                    @if($errors->has('extra_value'))
                        <span class="help-block">
                            <strong>{{$errors->first('extra_value') }}</strong>
                        </span>
                    @endif
                </div>
                    
                <div class="form-group {{ $errors->has('total_value') ? 'has-error' : ''}}">
                    <label for="total_value"> Valor total</label>
                    <input 
                        type="text" 
                        class="form-control valor" 
                        name="total_value"
                        value="{{old('total_value')}}"
                    />

                    @if($errors->has('total_value'))
                        <span class="help-block">
                            <strong>{{$errors->first('total_value') }}</strong>
                        </span>
                    @endif
                </div>
                
                <div class="form-group">
                    <label for="pass_through_payment_type_id">Tipo de pagamento</label>
                    <select name="pass_through_payment_type_id" id="pass_through_payment_type_id" class="form-control">
                        <option value="">Selecione</option>

                        @foreach(App\PassThroughPaymentType::all() as $pass_through_payment_type)
                            <option value="{{$pass_through_payment_type->id}}"
                                @if($pass_through_payment_type->id==old('pass_through_payment_type_id'))
                                    selected
                                @endif
                                >
                                {{$pass_through_payment_type->description}}
                            </option>
                        @endforeach

                    </select>
                </div>
                
                <div class="form-group">
                    <label for="pass_through_status_id">Status pagamento</label>
                    <select name="pass_through_status_id" id="pass_through_status_id" class="form-control">
                        <option value="">Selecione</option>

                        @foreach(App\PassThroughStatus::all() as $pass_through_status)
                            <option value="{{$pass_through_status->id}}"
                                @if($pass_through_status->id==old('pass_through_status_id'))
                                    selected
                                @endif
                                >
                                {{$pass_through_status->description}}
                            </option>
                        @endforeach

                    </select>
                </div>
                
                <div class="form-group {{ $errors->has('document') ? 'has-error' : ''}}">
                    <label for="document"> Documento</label>
                    <input 
                        type="text" 
                        class="form-control" 
                        name="document"
                        value="{{old('document')}}"
                    />

                    @if($errors->has('document'))
                        <span class="help-block">
                            <strong>{{$errors->first('document') }}</strong>
                        </span>
                    @endif
                </div>

                <button type="submit" class="btn btn-primary">Adicionar</button>
            </div>
        </form>
    </div>
</div>
<div class="clearfix"></div>
@endsection