@extends('adminlte::page')

@section('content_header')
@endsection

@section('content')

@push('css')
<style>
    .uper {
        margin-top: 40px;
    }
</style>
@if(session()->get('success'))
    <div class="alert alert-success">
    {{ session()->get('success') }}  
    </div><br />
@endif
<div class="col-md-6">
    
    <div class="box box-primary">
        <div class="box-header with-border">
            <h2 class="box-title">Repasses</h2>
            
        </div>
        <div class="box-body">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Associação</th>
                        <th>Competência</th>
                        <th>Valor</th>
                        <th width="40"> </th>
                    </tr>
                </thead>
                @foreach($pass_throughs as $pass_through)
                    <tr>
                        <td> {{$pass_through->association->initials}} </td>
                        <td> {{$pass_through->competence}} </td>
                        <td> {{$pass_through->total_value}} </td>
                        <td>
                            <a class="table-row btn btn-primary" href="{{ route('pass_throughs.show',Hashids::connection('pass_through')->encode($pass_through->id))}}">
                                Detalhes
                            </a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="box box-success">
        <div class="box-header with-border">
            <h2 class="box-title">Pesquisar(em construção)</h2>
        </div>
        <div class="box-body with-border">
            <div class="form-group">
                <input class="form-control" type="text" name="pesquisar" id="" placeholder="Pesquisar cliente">
            </div>
        </div>
        <div class="box-header with-border">
            <h2 class="box-title">Ações</h2>
        </div>
        <div class="box-body with-border">
            <a href={{ route('pass_throughs.create')}} class="btn btn-success">
                <span class="glyphicon glyphicon-plus"></span>
                Novo
            </a>
        </div>
        <div class="box-header with-border">
            <h2 class="box-title">Listar(em construção)</h2>
        </div>
        <div class="box-body">
            <a href="#" class="btn btn-danger">
                Inativos
            </a>
            <a href="#" class="btn btn-success">
                Ativos
            </a>
            <a href="#" class="btn btn-warning">
                Inadimplentes
            </a>
            <a href="#" class="btn btn-primary">
                Maiores
            </a>

        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection