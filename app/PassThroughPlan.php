<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PassThroughPlan extends Model
{
    protected $fillable=([
        'description',
    ]);

    public function passThroughs() {
        return $this->hasMany(PassThrough::class);
    }
}
