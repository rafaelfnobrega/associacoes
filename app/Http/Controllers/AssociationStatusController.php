<?php

namespace App\Http\Controllers;

use App\AssociationStatus;
use Illuminate\Http\Request;

class AssociationStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssociationStatus  $associationStatus
     * @return \Illuminate\Http\Response
     */
    public function show(AssociationStatus $associationStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssociationStatus  $associationStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(AssociationStatus $associationStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssociationStatus  $associationStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssociationStatus $associationStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssociationStatus  $associationStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssociationStatus $associationStatus)
    {
        //
    }
}
