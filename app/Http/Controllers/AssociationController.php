<?php

namespace App\Http\Controllers;

use App\Association;
use Illuminate\Http\Request;
use Hashids\Hashids;

class AssociationController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('association.index')->with(['associations'=>Association::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('association.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'initials' => "required",
            'name' => "required",
            'cnpj' => "required",
            'campus' => "required",
            'cep' => "required",
            'full_adress' => "required",
            'complement' => "required",
            'neighborhood' => "required",
            'city' => "required",
            'state' => "required",
            'phone' => "required",
            'phone2' => "required",
            'cellphone' => "required",
            'email' => "required",
            'email2' => "required",
            'contact' => "required",
        ]);

        $association = new Association([
            'initials' => $request->get('initials'),
            'name' => $request->get('name'),
            'cnpj' => $request->get('cnpj'),
            'campus' => $request->get('campus'),
            'cep' => $request->get('cep'),
            'full_adress' => $request->get('full_adress'),
            'complement' => $request->get('complement'),
            'neighborhood' => $request->get('neighborhood'),
            'city' => $request->get('city'),
            'state' => $request->get('state'),
            'phone' => $request->get('phone'),
            'phone2' => $request->get('phone2'),
            'cellphone' => $request->get('cellphone'),
            'email' => $request->get('email'),
            'email2' => $request->get('email2'),
            'contact' => $request->get('contact'),
            'association_status_id' => 1,
        ]);
        $association->save();
        
        
        return view('association.show')->with(['association'=>$association,'id_hashed'=>$id_hashed]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Association  $association
     * @return \Illuminate\Http\Response
     */
    public function show(Association $association)
    {

        return view('association.show')->with(['association'=>$association]);        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Association  $association
     * @return \Illuminate\Http\Response
     */
    public function edit(Association $association)
    {
        return view('association.edit')->with(['association'=>$association]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Association  $association
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Association $association)
    {
        $request->validate([
            'initials' => "required",
            'name' => "required",
            'cnpj' => "required",
            'campus' => "required",
            'cep' => "required",
            'full_adress' => "required",
            'complement' => "required",
            'neighborhood' => "required",
            'city' => "required",
            'state' => "required",
            'phone' => "required",
            'phone2' => "required",
            'cellphone' => "required",
            'email' => "required",
            'email2' => "required",
            'contact' => "required",
            'association_status_id' => "required",
        ]);

        $association->initials = $request->get('initials');
        $association->name = $request->get('name');
        $association->cnpj = $request->get('cnpj');
        $association->campus = $request->get('campus');
        $association->cep = $request->get('cep');
        $association->full_adress = $request->get('full_adress');
        $association->complement = $request->get('complement');
        $association->neighborhood = $request->get('neighborhood');
        $association->city = $request->get('city');
        $association->state = $request->get('state');
        $association->phone = $request->get('phone');
        $association->phone2 = $request->get('phone2');
        $association->cellphone = $request->get('cellphone');
        $association->email = $request->get('email');
        $association->email2 = $request->get('email2');
        $association->contact = $request->get('contact');
        $association->association_status_id = $request->get('association_status_id');

        $association->save();

        return view('association.show',['association'=>$association]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Association  $association
     * @return \Illuminate\Http\Response
     */
    public function destroy(Association $association)
    {
        //
    }
}
