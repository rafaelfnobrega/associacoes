<?php

namespace App\Http\Controllers;

use App\PassThrough;
use Illuminate\Http\Request;

class PassThroughController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pass_through.index')->with(['pass_throughs'=>PassThrough::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pass_through.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PassThrough  $passThrough
     * @return \Illuminate\Http\Response
     */
    public function show(PassThrough $passThrough)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PassThrough  $passThrough
     * @return \Illuminate\Http\Response
     */
    public function edit(PassThrough $passThrough)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PassThrough  $passThrough
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PassThrough $passThrough)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PassThrough  $passThrough
     * @return \Illuminate\Http\Response
     */
    public function destroy(PassThrough $passThrough)
    {
        //
    }
}
