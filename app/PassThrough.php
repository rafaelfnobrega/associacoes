<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PassThrough extends Model
{
    protected $fillable=([
        'association_id',
        'payment_date',
        'competence',
        'pass_through_plan_id',
        'holders_amount',
        'unitary_value',
        'extra_value',
        'total_value',
        'pass_through_payment_type_id',
        'pass_through_status_id',
        'document',
    ]);

    public function passThroughPaymentType() {
        return $this->belongsTo(PassThroughPaymentType::class);
    }

    public function association() {
        return $this->belongsTo(Association::class);
    }

    public function passThroughStatus() {
        return $this->belongsTo(PassThroughStatus::class);
    }

    public function passThroughPlan() {
        return $this->belongsTo(PassThroughPlan::class);
    }

}
