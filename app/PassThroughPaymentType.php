<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PassThroughPaymentType extends Model
{
    protected $fillable=([
        'description',
    ]);

    public function passThroughs() {
        return $this->hasMany(PassThrough::class);
    }
}
