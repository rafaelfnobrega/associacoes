<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\Hashidable;

class Association extends Model
{
    use Hashidable;
    
    protected $fillable=([
        'initials',
        'name',
        'cnpj',
        'campus',
        'cep',
        'full_adress',
        'complement',
        'neighborhood',
        'city',
        'state',
        'phone',
        'phone2',
        'cellphone',
        'email',
        'email2',
        'contact',
        'association_status_id',
    ]);

    public function associationStatus() {
        return $this->belongsTo(associationStatus::class);
    }
}